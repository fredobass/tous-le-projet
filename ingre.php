<?php 
$req = $bdd->query('SELECT * FROM ingredients ORDER BY id asc ');
$donnees = $req->fetchAll();
 ?>

<div class="container">
<hr>

<div class="row">
    
    <?php foreach ($donnees as $ingredients) : ?>
    <div class="col-md-3 text-center">
      <form method="POST">
       <input type="checkbox" id="" name="ingredient[]" value="<?= $ingredients['nom']; ?>">
       
      <img src="ingredients/<?= $ingredients['image']; ?>" width="150px" height="150px">
      <br>
      <strong><?php echo ucfirst($ingredients['nom']); ?></strong>
      <br>
 
    </div>
<?php endforeach ?>

</div>
<hr>

<input type="submit" name="envoyer" class="btn btn-success">
 </form>

</div>
<br>